# 文档目录
本文档由 [hexojs/site](https://github.com/hexojs/site) 下的中文markdown doc文档整理而来，因为我实在受不了 [hexo.io](https://hexo.io/) 时不时抽风打不开的问题了。由于语法上的细微差异可能有部分引用、代码块、超链接错误，造成的一切后果与本人无关。同时欢迎发现问题的同学及时指正，本人一定第一时间修复错误。

- ### 开始使用

1. [概述](doc/index.md)

2. [建站](doc/setup.md)

3. [配置](doc/configuration.md)

4. [命令](doc/commands.md)

5. [迁移](doc/migration.md)

- ### 基本操作

1. [写作](doc/writing.md)

2. [Front-matter](doc/front-matter.md)

3. [标签插件](doc/tag-plugins.md)

4. [资源文件夹](doc/asset-folders.md)

5. [数据文件夹](doc/data-files.md)

6. [服务器](doc/server.md)

7. [生成器](doc/generating.md)

8. [部署](doc/deployment.md)

- ### 自定义

1. [永久链接](doc/permalinks.md)

2. [主题](doc/themes.md)

3. [模板](doc/templates.md)

4. [变量](doc/variables.md)

5. [辅助函数](doc/helpers.md)

6. [国际化（i18n）](doc/internationalization.md)

7. [插件](doc/plugins.md)

- ### 其他

1. [问题解答](doc/troubleshooting.md)

2. [贡献](doc/contributing.md)